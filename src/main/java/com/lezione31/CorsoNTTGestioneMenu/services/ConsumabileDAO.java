package com.lezione31.CorsoNTTGestioneMenu.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione31.CorsoNTTGestioneMenu.connessioni.ConnettoreDB;
import com.lezione31.CorsoNTTGestioneMenu.model.Consumabile;

public class ConsumabileDAO implements Dao<Consumabile> {

	private Integer recuperaIdTipologia(String varNomeTipologia) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String queryTipologia = "SELECT tipologiaid FROM TipologiaConsumabile WHERE nome = ?";
       	PreparedStatement psTipologia = (PreparedStatement) conn.prepareStatement(queryTipologia);
       	psTipologia.setString(1, varNomeTipologia);
       	
       	ResultSet rsTipologia = psTipologia.executeQuery();
       	rsTipologia.next();
       	
       	return rsTipologia.getInt(1);
	}
	
	@Override
	public Consumabile getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
	    
	   	String query = "SELECT 	Consumabile.ConsumabileId, "
		       			+ "		Consumabile.codice, "
		       			+ "		Consumabile.nome, "
		       			+ "		Consumabile.descrizione, "
		       			+ "        TipologiaConsumabile.nome AS tipologia"
		       			+ "	FROM Consumabile"
		       			+ "	JOIN TipologiaConsumabile ON Consumabile.tipologia = TipologiaConsumabile.TipologiaId"
		       			+ " WHERE Consumabile.ConsumabileId = ?";
	   	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	   	ps.setInt(1, id);
	   	
	   	ResultSet risultato = ps.executeQuery();
	   	risultato.next();
	
			Consumabile temp = new Consumabile();
			temp.setId(risultato.getInt(1));
			temp.setCodice(risultato.getString(2));
			temp.setNome(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			temp.setTipologia(risultato.getString(5));
	   	
	   	return temp;	}

	public Consumabile getByCodice(String varCodice) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
	    
	   	String query = "SELECT 	Consumabile.ConsumabileId, "
		       			+ "		Consumabile.codice, "
		       			+ "		Consumabile.nome, "
		       			+ "		Consumabile.descrizione, "
		       			+ "        TipologiaConsumabile.nome AS tipologia"
		       			+ "	FROM Consumabile"
		       			+ "	JOIN TipologiaConsumabile ON Consumabile.tipologia = TipologiaConsumabile.TipologiaId"
		       			+ " WHERE Consumabile.codice = ?";
	   	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	   	ps.setString(1, varCodice);
	   	
	   	ResultSet risultato = ps.executeQuery();
	   	risultato.next();
	
			Consumabile temp = new Consumabile();
			temp.setId(risultato.getInt(1));
			temp.setCodice(risultato.getString(2));
			temp.setNome(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			temp.setTipologia(risultato.getString(5));
	   	
	   	return temp;
	}
	
	@Override
	public ArrayList<Consumabile> getAll() throws SQLException {
		ArrayList<Consumabile> elenco = new ArrayList<Consumabile>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT 	Consumabile.ConsumabileId, "
       			+ "		Consumabile.codice, "
       			+ "		Consumabile.nome, "
       			+ "		Consumabile.descrizione, "
       			+ "        TipologiaConsumabile.nome AS tipologia"
       			+ "	FROM Consumabile"
       			+ "	JOIN TipologiaConsumabile ON Consumabile.tipologia = TipologiaConsumabile.TipologiaId";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Consumabile temp = new Consumabile();
       		temp.setId(risultato.getInt(1));
       		temp.setCodice(risultato.getString(2));
       		temp.setNome(risultato.getString(3));
       		temp.setDescrizione(risultato.getString(4));
       		temp.setTipologia(risultato.getString(5));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Consumabile t) throws SQLException {
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		//TODO: Trasformare con una function di SQL l'assegnazione automatica dell'ID della tipologia all'interno del consumabile
   		
   		String query = "INSERT INTO consumabile (codice, nome, descrizione, tipologia) VALUES (?,?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	ps.setInt(4, recuperaIdTipologia(t.getTipologia()));		//Richiamo la funzione di recupero dell'ID
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Consumabile t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM consumabile WHERE consumabileid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public Consumabile update(Consumabile t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE consumabile SET "
   				+ "codice = ?, "
   				+ "nome = ?, "
   				+ "descrizione = ?, "
   				+ "tipologia = ? "
   				+ "WHERE consumabileid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	ps.setInt(4, recuperaIdTipologia(t.getTipologia()));
       	ps.setInt(5, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return getById(t.getId());
       	
   		return null;
	}
	
}
