package com.lezione31.CorsoNTTGestioneMenu;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lezione31.CorsoNTTGestioneMenu.model.Consumabile;
import com.lezione31.CorsoNTTGestioneMenu.services.ConsumabileDAO;

@RestController
@CrossOrigin(origins = {"http://localhost:4200", "http://localhost:8081"})
@RequestMapping("/consumabili")
public class ConsumabiliController {

	@GetMapping("/list")
	public ArrayList<Consumabile> restituisciTuttiConsumabili(){
		ArrayList<Consumabile> elenco = new ArrayList<Consumabile>();
		
		ConsumabileDAO conDao = new ConsumabileDAO();
		try {
			elenco = conDao.getAll();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return elenco;
	}
	
	@GetMapping("/{codice}")
	public Consumabile restituisciConsumabile(@PathVariable String codice) {
		Consumabile temp = null;
		
		ConsumabileDAO conDao = new ConsumabileDAO();
		try {
			temp = conDao.getByCodice(codice);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
		
		return temp;
	}
	
	@PostMapping("/insert")
	public Consumabile inserisciConsumabile(@RequestBody Consumabile cons) {
		Consumabile temp = null;
		
		ConsumabileDAO conDao = new ConsumabileDAO();

		try {
			conDao.insert(cons);
			temp = cons;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
	}
	
	@DeleteMapping("/delete/{codice}")
	public boolean eliminaConsumabile(@PathVariable String codice) {
		boolean risultatoEliminazione = false;
		
		ConsumabileDAO conDao = new ConsumabileDAO();
		
		try {
			Consumabile cons = conDao.getByCodice(codice);
			if(cons.getTipologia().equals("Bevanda"))			//Posso eliminare solo le bevande
				risultatoEliminazione = conDao.delete(cons);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return risultatoEliminazione;
	}
	
	@PutMapping("/update/{codice}")
	public Consumabile aggiornaConsumabile(@PathVariable String codice, @RequestBody Consumabile objCons) {
		Consumabile temp = null;

		ConsumabileDAO conDao = new ConsumabileDAO();
		
		try {
			Consumabile consDatabase = conDao.getByCodice(codice);
			
			consDatabase.setCodice(objCons.getCodice());
			consDatabase.setNome(objCons.getNome());
			consDatabase.setDescrizione(objCons.getDescrizione());
			consDatabase.setTipologia(objCons.getTipologia());
			
			temp = conDao.update(consDatabase);
			
			temp.setId(null);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
		return temp;
	}
}
