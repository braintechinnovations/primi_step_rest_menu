package com.lezione31.CorsoNTTGestioneMenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorsoNttGestioneMenuApplication {

	public static void main(String[] args) {
		SpringApplication.run(CorsoNttGestioneMenuApplication.class, args);
	}

}
