package com.lezione31.CorsoNTTGestioneMenu.model;

public class Consumabile {

	private Integer id;
	private String codice;
	private String nome;
	private String descrizione;
	private String tipologia;
	
	public Consumabile() {
		
	}
	
	public Consumabile(Integer id, String codice, String nome, String descrizione, String tipologia) {
		super();
		this.id = id;
		this.codice = codice;
		this.nome = nome;
		this.descrizione = descrizione;
		this.tipologia = tipologia;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	
	
	
}
